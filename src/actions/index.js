import {
    ADD_ITEM, REMOVE_ITEM, CHANGE_STATUS_ITEM
} from '../constants';

export const addToDo = (task) => {
    return {
        type: ADD_ITEM,
        payload: task
    }
};

export const removeToDo = (id) => {
    return {
        type: REMOVE_ITEM,
        payload: id
    }
};
export const changeStatusToDo = (id) => {
    return {
        type: CHANGE_STATUS_ITEM,
        payload: id
    }
};