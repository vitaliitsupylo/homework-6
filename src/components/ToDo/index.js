import React from "react";
import {connect} from "react-redux";

import Button from "./../Button";
import Checkbox from "../Checkbox";
import {changeStatusToDo, removeToDo} from "../../actions";

import './style.scss';

const ToDo = ({tasks, checkedTask, deleteTask}) => {
    return (
        <div className="items">
            {tasks.map((task) => {
                return (<div className={task.status ? 'item checked' : 'item'} key={task.id}>
                    <Checkbox type="checkbox" status={task.status} handler={checkedTask(task.id)}/>
                    <p className="item__text">{task.text}</p>
                    <Button type="button" handler={deleteTask(task.id)}>&#10006;</Button>
                </div>)
            })}
        </div>
    )
};


const mapStateToProps = (state) => ({
    tasks: state.tasks
});

const mapDispatchToProps = (dispatch) => ({
    checkedTask: (id) => () => {
        dispatch(changeStatusToDo(id));
    },
    deleteTask: (id) => () => {
        dispatch(removeToDo(id));
    },

});

export default connect(mapStateToProps, mapDispatchToProps)(ToDo);