import React from "react";
import PropTypes from "prop-types";
import './style.scss';

const Input = ({type, value, handler}) => {
    return <input type={type} value={value} onChange={handler} className="input"/>;
};

Input.propTypes = {
    type: PropTypes.oneOf(['text', 'email']).isRequired,
    value: PropTypes.string.isRequired,
    handler: PropTypes.func.isRequired
};

Input.defaultProps = {
    type: 'text',
    value: '',
    handler() {
        console.log('no handler')
    }
};

export default Input;