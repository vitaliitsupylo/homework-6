import React from "react";
import './style.scss'
import PropTypes from "prop-types";


const Checkbox = ({type, handler, status}) => {
    return <input type={type} className="checkbox" checked={status} onChange={handler}/>
};

export default Checkbox

Checkbox.propTypes = {
    type: PropTypes.oneOf(['radio', 'checkbox']).isRequired,
    handler: PropTypes.func.isRequired
};

Checkbox.defaultProps = {
    type: 'checkbox',
    handler() {
        console.log('no handler')
    }
};