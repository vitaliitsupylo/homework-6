import React from "react";
import PropTypes from 'prop-types';
import './style.scss';


function Button({type, children, handler}) {
    return (
        <button className="button" type={type} onClick={handler}>
            <span>{children}</span>
        </button>
    )
}

Button.propTypes = {
    children: PropTypes.string.isRequired,
    type: PropTypes.oneOf(['button', 'submit']).isRequired,
    handler: PropTypes.func.isRequired
};

Button.defaultProps = {
    type: 'button',
    children: 'Button',
    handler() {
        console.log('no handler')
    }
};

export default Button;
