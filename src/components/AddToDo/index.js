import React, {useState} from "react";
import {connect} from "react-redux";

import Button from "../Button";
import Input from "../Input";
import {addToDo} from '../../actions';

import './style.scss';


const AddToDo = ({addNewTask}) => {
    const [value, setValue] = useState('');

    const createId = () => Date.now();

    const changeValue = (even) => {
        setValue(even.target.value);
    };

    const setDataTask = () => {
        addNewTask({id: createId(), text: value, status: false});
        setValue('');
    };

    return (
        <div className="add-todo">
            <Input type="text" value={value} handler={changeValue}/>
            <Button handler={setDataTask}>&#10003;</Button>
        </div>
    )
};

const mapStateToProps = (state) => ({
    tasks: state.tasks
});

const mapDispatchToProps = (dispatch) => ({
    addNewTask: (task) => {
        dispatch(addToDo(task));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(AddToDo);
