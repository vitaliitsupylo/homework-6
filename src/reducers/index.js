import {
    ADD_ITEM, REMOVE_ITEM, CHANGE_STATUS_ITEM
} from '../constants';

const initialState = {
    tasks: []
};

function reducer(state = initialState, action) {
    switch (action.type) {

        case ADD_ITEM:
            return {
                tasks: [
                    ...state.tasks,
                    action.payload
                ]
            };
        case REMOVE_ITEM:
            return {
                tasks: state.tasks.filter(task => task.id !== action.payload)
            };
        case CHANGE_STATUS_ITEM:
            return {
                tasks: state.tasks.map(task => {
                    const {...newArr} = task;
                    if (newArr.id === action.payload) {
                        newArr.status = !newArr.status;
                    }
                    return newArr;
                })
            };
        default:
            return state;
    }
}

export default reducer;
