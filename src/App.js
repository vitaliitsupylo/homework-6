import React from 'react';
import {Provider} from 'react-redux';

import store from './redux/store';
import ToDo from "./components/ToDo";
import AddToDo from "./components/AddToDo";

import './App.scss';


function App() {
    return (
        <Provider store={store}>
            <div className="to-do">
                <p className="to-do__title">To do</p>
                <ToDo/>
                <AddToDo/>
            </div>
        </Provider>
    );
}

export default App;
